package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	private static final int MAX_ROLLS = 20;
	private int[] rolls = new int[20];
	private int bonus = 0; 
	private int currentRoll = 0;
	private int perfectGame = 0; // n° strike
	@Override
	public void roll(int pins) {
		if(currentRoll < MAX_ROLLS){
			if( currentRoll > 0 && currentRoll < MAX_ROLLS-2 && isStrike(currentRoll - 1) ){
				rolls[currentRoll] = 0; //se avviene uno strike il prossimo tiro è 0
				currentRoll++;
			}
			rolls[currentRoll] = pins;
			currentRoll++;
		}else 
			if(currentRoll == MAX_ROLLS )
				bonus = pins;
		
		
	}

	@Override
	public int score() {
		// TODO Auto-generated method stub
		int punteggio = 0;
		
		for(int currentRoll=0; currentRoll<20; currentRoll++){
			if( isStrike(currentRoll) ){
				if(currentRoll == MAX_ROLLS-2){
					punteggio += bonus;
					if(bonus == 10)
						perfectGame++;
				}else if(currentRoll != MAX_ROLLS-1)
					punteggio += rolls[currentRoll+2] + rolls[currentRoll+3];
				perfectGame++;
			}else {
				if( isSpare(currentRoll) )
					punteggio += rolls[currentRoll+2];	
			}			
			punteggio += rolls[currentRoll];	
		}		
		if(perfectGame == 12)
				punteggio = 300;
		
		return punteggio;
	}
	
	public boolean isSpare(int currentRoll){
		return currentRoll < MAX_ROLLS -1 && rolls[currentRoll] + rolls [currentRoll+1] == 10 && currentRoll % 2 == 0;
	}
	
	public boolean isStrike(int currentRoll){
		//return currentRoll < MAX_ROLLS -1 && rolls[currentRoll] == 10 && currentRoll % 2 == 0;
		return currentRoll < MAX_ROLLS  && rolls[currentRoll] == 10;
	}

}
